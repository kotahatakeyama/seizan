//
//
//
// ブラウザーの幅に合うように調整
// $(window).load(function() {
//   var winWid;
//   $(window).on('load resize orientationchange', function(){
//       winWid = $(window).width();
//       if(winWid <= 960 ){
//         winWid = 960;
//       }
//       $(".pcMv01").css({'width':( winWid )});
//
//   });
// });
$(document).ready(function(){

//メインビジュアルのフェードイン・フェードアウト
  $(function(){
    var lineNum = $('.slider').children('li').length;
    var line = $('.slider li');
    var index = 0;
    $('.slider.active').eq(index).addClass('active');
    $('.slider li:gt(0)').hide();
      setInterval(function() {
      if( (lineNum-1) == index){
        line.eq(index).fadeOut('slow', function(){
          line.eq(index).removeClass('active');
        });
        index = 0;
        line.eq(index).fadeIn('slow');
        line.eq(index).addClass('active');
      }else {
        line.eq(index).fadeOut('slow', function(){
          line.eq(index).removeClass('active');
        });
        index++;
        line.eq(index).fadeIn('slow');
        line.eq(index).addClass('active');
      }
    },5000);
  });


  var wid = $(window).width();

  // 仕事紹介アコーディオン
  $(function(){
    if ( wid >= 759 ) {
      $("header .slideBtn").on("click", function() {
        $("header .sldCnt").fadeToggle(300);
      });
      $("footer .slideBtn").on("click", function() {
        $("footer .sldCnt").slideToggle(300);
      });
    }
  });

  // SPメニューの開閉
  $(function(){
    if ( wid <= 759 ) {
      $(".menuBtn01").on("click", function() {
        $(".headTop").toggle('fadeIn', '', 150);
        $(this).toggleClass('close');
      });
        $("header .slideBtn").on("click", function() {
          $("header .spSlide").slideToggle(300);
      });
    }
  });


  // 画面に表示されたらアニメーション
  $(function(){
    $(".lowVisual h1").addClass('scaleTtl');
    $(window).scroll(function (){
      $('.scale').each(function(){
        var imgPos = $(this).offset().top;
        var scroll = $(window).scrollTop();
        if (scroll > imgPos - 500){
          $(this).css({
            "transform": "scaleX(1)",
            "opacity": "1",
          });
        }
      });
    });
  });


  //SPメニュ−の高さコンテンツを下げる。
  $(function(){
    var wid = $(window).width();
    // mainVisualの高さ
    var mvHeight = wid / 2.42;
    // 下層lowVisualの高さ
    var lmHeight = wid *1.14;
    var lvHeight = wid / 2.3;
    var scrollTop = $(window).scrollTop();
    var scrollTop2= $(".sp_menu").innerHeight();
    // SP仕事紹介アンカーリンク高さ
    var qWid = $(".qLink li").width();
    var qHeight = qWid * 0.43;
    // spでgooglemapの高さ
    var mapWid = $(".shopDetail iframe").width();
    var mapHeight = mapWid * 0.67;
    // その他の仕事紹介高さ
    var bnrWid = $(".otherWork li").width();
    var bnrHeight = bnrWid * 0.275;
    // .働くひとたちの声高さ
    var workHeight = $(".workerList li:first-child").outerHeight();
      $(".mv01 .pcMv01").css("height", mvHeight);
    if ( wid <= 759 ) {
      //レスポンシブ画像切り替え
      $('img').each(function(){
        $(this).attr("src",$(this).attr("src").replace('_pc', '_sp'));
      });
      $(".mv01 .pcMv01").css("height", lmHeight);
      $(".lowVisual").css("height", lvHeight);
      $('.lowVisual h1').css("line-height", lvHeight + "px");
      $(".qLink li").css("height", qHeight);
      $(".shopDetail iframe").css("height", mapHeight);
      $(".otherWork li").css("height", bnrHeight);
      $('.mv01, .lowVisual, .wrapper').css("top", scrollTop2);
      $(".wrapper").css("margin-bottom", scrollTop2);
      $(footer).css("top", mvHeight);
      if ($(".workerList li:first-child").height() >=  $(".interviewer img").height()) {
        $(".interviewer img").css("height", workHeight);
      }
    }
  });
});
